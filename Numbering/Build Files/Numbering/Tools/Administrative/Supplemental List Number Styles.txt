The following number styles can be added to the dropdown list in the Edit Scheme dialog by adding their corresponding numeric values, separated by pipe characters, to the SupplementalListNumberStyles key in the [Numbering] section of mpn90.ini.

Aiueo = 20
Aiueo Half Width = 12 
Arabic 1 = 46
Arabic 2 = 48 
Arabic Full Width = 14 
Chosung = 25 
Ganada = 24
GB Numeric 1 = 26 
GB Numeric 2 = 27 
GB Numeric 3 = 28 
GB Numeric 4 = 29 
Hangul = 43 
Hanja = 44 
Hanja Read = 41
Hanja Read Digit = 42 
Hebrew 1 = 45
Hebrew 2 = 47 
Hindi Arabic = 51 
Hindi Cardinal Text = 52 
Hindi Letter 1 = 49 
Hindi Letter 2 = 50 
Iroha = 21 
Iroha Half Width  = 13
Kanji = 10 
Kanji Digit = 11 
Kanji Traditional = 16 
Kanji Traditional2 = 17
Lowercase Russian = 58 
Number In Circle = 18
Simplified Chinese 1 = 37
Simplified Chinese 2 = 38
Simplified Chinese 3 = 39 
Simplified Chinese 4 = 40
Thai Arabic = 54 
Thai Cardinal Text 55 
Thai Letter = 53 
Traditional Chinese 1 = 33 
Traditional Chinese 2 = 34 
Traditional Chinese 3 = 35 
Traditional Chinese 4 = 36 
Uppercase Russian = 59 
Vietnamese Cardinal Text = 56 
Zodiac 1 = 30
Zodiac 2 = 31 
Zodiac 3 = 32 
