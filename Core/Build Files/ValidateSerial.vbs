Option Explicit

Dim xSer
'Dim bRnd
Dim xCode
Dim xCodeMod
Dim i
Dim xDate
Dim x
Dim xPart1
Dim xPart2
Dim xPart3
Dim xSerial
Dim d
Dim bIsValid

Function ValidateSerial
xSer= Session.Property("LICENSEKEY")
xSer = Trim(xSer)
If Len(xSer) < 18 Then
	Session.Property("LICENSEKEYISVALID") = "0"
	'msgbox "Please enter a valid license key."
	'msgbox "Value for ProductID:  " & Session.Property("LICENSEKEY")
	ValidateSerial = 0
Else
'   unencrypt date

'   remove garbage 1st digit
    xCode = Mid(xSer, 2)

'    msgbox xCode

'   store and remove random factor
'    bRnd = Left(xCode, 1)
    xCode = Mid(xCode, 2)
    
'   collect chars at even indexes
    For i = 2 To Len(xCode) Step 2
        xCodeMod = xCodeMod & Mid(xCode, i, 1)
    Next
    
    While Len(xCodeMod)
        x = Left(xCodeMod, 1)
        If Not IsNumeric(x) Then
            If IsReserved(x) Then
                x = Asc(x) - 100
            Else
                'non-reserved letter, so get mapping for lower case
                x = MapLetter(LCase(x))
            End If
        End If
        
        xDate = xDate & x
        
'       trim left char
        xCodeMod = Mid(xCodeMod, 2)
    Wend
'    msgbox xDate
'    
    xPart1 = Mid(xDate, 3, 4)
    xPart2 = Mid(xDate, 7, 2)
    xPart3 = Mid(xDate, 1, 2)
    
'    msgbox xPart1 & " " & xPart2 & " " & xPart3
    
    'UnEncryptDate = DateSerial(xPart1, xPart2, xPart3)

    'msgbox DateSerial(xPart1, xPart2, xPart3)

    xSerial = DateSerial(xPart1, xPart2, xPart3)
    
'    msgbox xSerial
    
    d = xSerial
    
'    msgbox "d= " & d
    
'    msgbox d = #4/1/2099#
'    msgbox d > Now
'    msgbox CLng(d - Now) < 180
    
'   valid key?
    bIsValid = (d = #4/1/2099#) Or _
               (d > Now And (CLng(d - Now) < 361))

    'msgbox "bIsValid = " & bIsValid
    
    If bIsValid = True Then
	    Session.Property("LICENSEKEYISVALID") = "1"
	    Session.Property("LICENSEKEY") = xSer
	    ValidateSerial = 1
	    'msgbox "ValidateSerial = 1"
	Else
	    Session.Property("LICENSEKEYISVALID") = "0"
	    ValidateSerial = 0
	    'msgbox "ValidateSerial = 0"
	End If
End If
End Function

Function MapLetter (xLetter)

    Dim bDigit
    
    Select Case xLetter
    Case "q"
        bDigit = 9
    Case "r"
        bDigit = 8
    Case "s"
        bDigit = 7
    Case "t"
        bDigit = 6
    Case "u"
        bDigit = 5
    Case "v"
        bDigit = 4
    Case "w"
        bDigit = 3
    Case "x"
        bDigit = 2
    Case "y"
        bDigit = 1
    Case "z"
        bDigit = 0
    End Select
    
    MapLetter = bDigit

End Function

Function IsReserved (xLetter)

Select Case xLetter
Case "d", "e", "f", "g", "h", "i", "j", "k", "l", "m"
    IsReserved = True
Case Else
    IsReserved = False
End Select
End Function
